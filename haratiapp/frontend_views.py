from django.views.generic import ListView
from .models import Item, SubCategory

class ListProduct(ListView):
    model = Item
    template_name = 'haratiapp/frontend/products.html'
    paginate_by = 12
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subcategories_clothing'] = SubCategory.objects.filter(category='Clothing')
        context['subcategories_footwear'] = SubCategory.objects.filter(category='FootWear')
        context['subcategories_bagsandwallets'] = SubCategory.objects.filter(category='Bags&Wallets')
        context['subcategories_accessories'] = SubCategory.objects.filter(category='Accessories')
        context['subcategories_womenwears'] = SubCategory.objects.filter(category='WomenWears')
        context['items'] = Item.objects.all()
        context['paginate_by'] = self.paginate_by
        return context