from braces.views import (LoginRequiredMixin, SuperuserRequiredMixin, GroupRequiredMixin)
from django.core.urlresolvers import reverse_lazy

class AuthMixin(SuperuserRequiredMixin):
    login_url = reverse_lazy('haratiapp:login')

class EmployeeRequiredMixin(LoginRequiredMixin):
    login_url = reverse_lazy('haratiapp:homeindex')