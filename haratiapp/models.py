from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from multiselectfield import MultiSelectField

category = (
    ('Clothing','Clothing'), ('FootWear', 'FootWear'), ('Bags&Wallets', 'Bags&Wallets'), ('Accessories', 'Accessories'),
    ('WomenWears','WomenWears')
)


subcategory = (
    ('T-shirt','T-shirt'), ('Casual Shirt', 'Casual Shirt'), ('Half Pants', 'Half Pants'),
    ('Jeans','Jeans'),
    ('Trousers','Trousers'),
    ('Sportswear', 'Sportswear'),
    ('Sweaters', 'Sweaters'),
    ('Boxers', 'Boxers'),
    ('Thermal','Thermal'),
    ('Windcheater','Windcheater')
)


size = (
    ('Double-Extra-Large', 'XXL'),
    ('Extra-Large', 'XL'),
    ('Large', 'L'),
    ('Medium', 'M'),
    ('Small', 'S'),
)
# Create your models here.

quantity = (
    (1, '1'),
    (2, '2'),
    (3,'3'),
    (4,'4'),
    (5,'5'),
    ( 6,'6'),
    (7,'7'),(8,'8'),(9,'9'),(10, '10')
)

color = (
    ('Red','Red'),
    ('Blue', 'Blue'),
    ('Black', 'Black'),
    ('Yellow', 'Yellow'),
    ('White', 'White'),
    ('Green', 'Green')

)

status = (
    (1, 'Yes'),
    (0, 'No')
)

class Time(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, )
    updated_at = models.DateTimeField(auto_now_add=False, auto_now=True, )
    deleted_at = models.DateTimeField(null=True, blank=True)
    class Meta:
        abstract = True

    def delete(self,*args,**kwargs):
        self.deleted_at = timezone.now()
        super().save()


class AboutUs(models.Model):
    address = models.CharField(max_length=100)
    landline_phone= models.CharField(max_length=15)
    mobile_phone = models.CharField(max_length=15)
    email = models.EmailField()

    class Meta:
        verbose_name_plural = "About Us"

    def __str__(self):
        return self.address


################################################# NEW CONCEPT ############################################################



class SubCategory(models.Model):
    category=models.CharField(choices=category, max_length=100)
    subcategory=models.CharField(max_length=100)

    def __str__(self):
        return self.subcategory

class Item(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True,null=True)
    item_number = models.CharField(max_length=25,blank=True,null=True)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE, blank=True, null=True)
    size = models.CharField(choices=size, max_length=25,blank=True,null=True)
    color = models.CharField(choices=color, max_length=25,blank=True,null=True)
    price = models.IntegerField(blank=True,null=True)
    status = models.BooleanField(choices=((True,'Publish'),(False,'Dont Publish')), default=False)
    photo = models.ImageField(upload_to='media_cdn', blank=True,null=True)
    def __str__(self):
        return self.name


class Featured(models.Model):
    featured = models.ForeignKey(Item)


class Trending(models.Model):
    trending = models.ForeignKey(Item)