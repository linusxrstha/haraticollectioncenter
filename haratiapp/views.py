from django.views.generic import TemplateView, FormView, ListView
from django.views.generic.edit import CreateView
from .forms import LoginForm, RegisterForm, AddProduct
from django.http import HttpResponseRedirect, HttpResponse
from .mixins import *
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from .models import Item
from django.views import View
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from haratiapp.tokens import account_activation_token
from django.core.mail import EmailMessage
from django.urls import reverse



class AdminIndex(EmployeeRequiredMixin, TemplateView):
    template_name = 'haratiapp/dashboard/dashboard.html'


class HomeIndex(EmployeeRequiredMixin ,TemplateView):
    print('home index')
    template_name = 'haratiapp/frontend/homepage.html'


class Index(TemplateView):
    print('just index')
    template_name = 'haratiapp/frontend/homepage.html'


class AdminLogin(TemplateView):
    template_name = 'haratiapp/dashboard/dashboard.html'

class Login(FormView):

    template_name = 'haratiapp/frontend/login.html'
    form_class = LoginForm

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(self.request, user)
            print('User logged in !!')
            if user.is_superuser:
                print('Superuser Detected !!')
                return HttpResponseRedirect('/admin-login')
            print('outside the if')
            return HttpResponseRedirect(reverse('haratiapp:index'))

        else:
            print('An error has occured !!')
            messages.error(self.request, 'Please check your username and password !!')
            return HttpResponseRedirect('/login')



class Logout(EmployeeRequiredMixin, TemplateView):
    def get(self, request):
        logout(request)
        print('user logged out !!')
        return HttpResponseRedirect('/login/')


class Register(FormView):
    template_name = 'haratiapp/dashboard/register.html'
    form_class = RegisterForm

    def form_valid(self, form):
        if form.cleaned_data['password'] == form.cleaned_data['repassword']:
            User.objects.create_user(username = form.cleaned_data['username'], password=form.cleaned_data['password'],
                             email = form.cleaned_data['email'])
        else:
            messages.error(self.request, 'Password do not match!! Please check your passwords and enter again !!')
            return HttpResponseRedirect('/register/')
        return HttpResponseRedirect('/login/')




class Create_Product(CreateView):
    model = Item
    template_name = 'haratiapp/dashboard/create_product.html'
    form_class = AddProduct
    success_url = reverse_lazy('haratiapp:admin_list_product')

    def form_valid(self, form):
        print('here is the key')
        form.save()
        return super().form_valid(form)


class List_Product(ListView):
    model = Item
    template_name = 'haratiapp/dashboard/list_product.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['paginate_by'] = self.paginate_by
        print(self.paginate_by)
        return context


