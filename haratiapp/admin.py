from django.contrib import admin
from .models import  Item, Featured, Trending, SubCategory

# Register your models here.
admin.site.register(Item)
admin.site.register(Featured)
admin.site.register(Trending)
admin.site.register(SubCategory)

