jQuery(document).ready(function() {
    var x = $("#quickmenu").offset();
    var y = $('#choose').offset();
    jQuery("#quickmenu").wrap('<div class="nav-placeholder"></div>');
    jQuery(".nav-placeholder").height(jQuery("#quickmenu").outerHeight());

    jQuery(window).scroll(function() {
        var scrolltop = jQuery(window).scrollTop();

        if ( scrolltop >= x.top && scrolltop <=y.top ) {
            jQuery("#quickmenu").addClass('fixed');
            jQuery(".item").addClass('reduce');
        } else {
            jQuery("#quickmenu").removeClass('fixed');
            jQuery(".item").removeClass('reduce');

        }
    });

    jQuery('#choose .featured').click(function() {
        console.log('shoes clicked !!')
        jQuery('#featured-content').css({
            'display': 'grid',
        });

        jQuery('#latest-content').css({
            'display': 'none'
        });
        jQuery('#trending-content').css({
            'display': 'none'
        });
        jQuery('.featured').css({
            'border-bottom': '5px solid black'
        });
        jQuery('.latest').css({
            'border-bottom': '0'
        });
        jQuery('.trending').css({
            'border-bottom': '0'
        });


    });

    jQuery('#choose .latest').click(function() {
        console.log('shoes clicked !!')
        jQuery('#featured-content').css({
            'display': 'none'
        });

        jQuery('#latest-content').css({
            'display': 'grid'
        });
        jQuery('#trending-content').css({
            'display': 'none'
        });

        jQuery('.featured').css({
            'border-bottom': '0'
        });
        jQuery('.latest').css({
            'border-bottom': '5px solid black'
        });
        jQuery('.trending').css({
            'border-bottom': '0'
        });
    });

    jQuery('#choose .trending').click(function() {
        console.log('shoes clicked !!')
        jQuery('#featured-content').css({
            'display': 'none'
        });

        jQuery('#latest-content').css({
            'display': 'none'
        });
        jQuery('#trending-content').css({
            'display': 'grid'
        });

        jQuery('.featured').css({
            'border-bottom': '0'
        });
        jQuery('.latest').css({
            'border-bottom': '0'
        });
        jQuery('.trending').css({
            'border-bottom': '5px solid black'
        });
    });


});
