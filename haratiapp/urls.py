from django.conf.urls import url, include
from .views import AdminIndex, Index, Login, Logout, AdminLogin, Register, Create_Product,List_Product
from .frontend_views import ListProduct


app_name = 'haratiapp'

urlpatterns = [
    #url(r'^$', AdminIndex.as_view(), name = 'adminindex'),

    url(r'^$', Index.as_view(), name = 'index'),


    url(r'^login/$', Login.as_view(), name = 'login'),
    url(r'^logout$', Logout.as_view(), name = 'logout'),
    url(r'^register/$', Register.as_view(), name='register'),



    url(r'^admin-login$', AdminLogin.as_view(), name = 'admin-login'),
    url(r'^admin/createproduct$', Create_Product.as_view(), name='admin_create_product'),
    url(r'^admin/listproduct', List_Product.as_view(), name='admin_list_product'),
    url(r'^listproduct', ListProduct.as_view(), name='listproduct'),


]
