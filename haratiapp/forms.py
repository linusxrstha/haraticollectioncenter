from django import forms
from .models import Item

class LoginForm(forms.Form):
    username = forms.CharField(
    widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Enter Email',
        }
    )
    )
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Enter Password',
        }
    ))

class RegisterForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Enter your username'
            }
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Enter your password'
            }
        )
    )
    repassword = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Re-Enter your password'
            }
        )
    )
    email = forms.CharField(
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Enter your email'
            }
        )
    )


class AddProduct(forms.ModelForm):
   class Meta:
        model = Item
        fields = ['name', 'description',
                  'item_number', 'size', 'color', 'subcategory', 'price',
                  'status']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter product name',
                'required': 'True',
                            }),
            'description': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter description of your product.',

                            }),
            'item_number': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter item number',

            }),
            'size': forms.Select(attrs={
                'class': 'form-control borderline',
                'placeholder': 'Enter Size',


            }),
             'color': forms.Select(attrs={
                 'class': 'form-control',
                 'placeholder': 'Color',

             }),


            'subcategory': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Subcategory',

            }),

            'price': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Price',

            }),

            'status': forms.Select(attrs={
                'class': 'form-control',
              }),
        }

