
from rest_framework import serializers
from haratiapp.models import Item
class ProductApiSerializer(serializers.ModelSerializer):
    class Meta:

        fields = [
            'id',
            'name',
            'description',
            'price',
        ]
        model = Item

    def validate_name(self,value):
        qs = Item.objects.filter(name__iexact = value)
        if qs.exists():
            raise serializers.ValidationError("The name must be uniqe")
        return value

class ItemApiSerializer(serializers.ModelSerializer):
    class Meta:

        fields = [
            'id',
            'size',
            'color',
            'item_number',
        ]
        model = Item