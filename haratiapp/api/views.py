from rest_framework import generics, mixins
from haratiapp.models import Item
from .serializers import ProductApiSerializer, ItemApiSerializer
from rest_framework import permissions

# class ListProductApi(mixins.CreateModelMixin, generics.ListAPIView):
#     queryset = Prod.objects.all()
#     serializer_class = ProductApiSerializer
#     permission_classes = []
#
#     def post(self, request, *args, **kwargs):
#         return self.create( request, *args, **kwargs)
#
#
#
# class RetrieveUpdateDeleteProductApi(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Prod.objects.all()
#     serializer_class = ProductApiSerializer

class ListProductApi(generics.ListCreateAPIView):
    queryset = Item.objects.all()
    serializer_class = ProductApiSerializer


class RetrieveUpdateDeleteProductApi(generics.RetrieveUpdateDestroyAPIView):
    queryset = Item.objects.all()
    serializer_class = ProductApiSerializer

class ListItemAPi(generics.ListAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemApiSerializer
