from django.conf.urls import url
from .views import ListProductApi, RetrieveUpdateDeleteProductApi, ListItemAPi

urlpatterns = [
    url(r'^product/$', ListProductApi.as_view(), name = 'listproduct-api'),
    url(r'^product/(?P<pk>\d+)/$', RetrieveUpdateDeleteProductApi.as_view(), name = 'listproduct-api'),
    url(r'^item/$', ListItemAPi.as_view(), name='listitem-api'),

   ]

